'use strict';

/**
 * @param {Egg.Application} app - egg application
 */
module.exports = app => {
  const { router, controller, middleware } = app;
  const _jwt = middleware.jwtErr(app.config.jwt.secret); // 传入加密字符串
  router.get('/home', controller.home.index);
  router.post('/api/user/register', controller.user.register);
  router.post('/api/user/login', controller.user.login);
  // 使用中间件验证token,需要token验证的加_jwt
  router.get('/api/user/testToken', _jwt, controller.user.testToken);
  router.get('/api/user/getUserInfo', _jwt, controller.user.getUserInfo);
  router.post('/api/upload', _jwt, controller.upload.upload);
  router.post('/api/user/editUserInfo', _jwt, controller.user.editUserInfo);
  router.post('/api/user/editUserPas', _jwt, controller.user.editUserPas);
  router.post('/api/bill/addBill', _jwt, controller.bill.addBill);
  router.get('/api/bill/list', _jwt, controller.bill.billList);
  router.get('/api/bill/details', _jwt, controller.bill.details);
  router.post('/api/bill/update', _jwt, controller.bill.update);
  router.post('/api/bill/delete', _jwt, controller.bill.delete);
  router.get('/api/bill/data', _jwt, controller.bill.data); // 获取数据
  router.get('/api/bill/data', _jwt, controller.bill.data); // 获取数据
  router.get('/api/type/getType', _jwt, controller.type.getType); // 获取数据
  router.post('/api/money/addMoney', _jwt, controller.money.addMoney); // 获取数据
  router.get('/api/money/getMoney', _jwt, controller.money.getMoney); // 获取数据
  // http 接口, 在对应的控制器中可以直接操作socket, 非常方便
  // router.get('/iotest', app.io.controller.chat.iotest);
  // router.get('/ioclient', app.io.controller.chat.ioclient);
  // // socket, 指向app/io/controller/chat.js的index方法 , of 是划分命名空间
  // // 对于在命名空间"/" 下,监听到的 chat 事件将由 app.io.controller.chat.index 中 chat 方法处理，chat 方法可以前端可以通过调用 socket.emit('chat',{name:'jjj'}),可以将数据传递到app.io.controller.chat.index 中 index 方法中进行处理
  // io.of('/').route('chat', app.io.controller.chat.index); // io.of('/').router('event'，handler)
  // io.of('/').route('wechat', app.io.controller.chat.wechat); // 触发的wechat事件，交给 app.io.controller.chat.wechat处理

};
