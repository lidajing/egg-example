'use strict';

const moment = require('moment');
const Controller = require('egg').Controller;

class BillController extends Controller {
  async addBill() {
    const { ctx, app } = this;
    let token = ctx.request.header.authorization; // 请求头获取 authorization 属性，值为 token
    const { amount, type_id, type_name, pay_type, remark, date } = ctx.request.body;
    if (!amount || !type_id || !type_name || !pay_type || !date) {
      ctx.body = {
        code: 400,
        message: '参数错误!',
      };
      return;
    }
    try {
      // 这里去除后才能解析出来
      if (token.indexOf('Bearer ') !== -1) {
        token = token.replace('Bearer ', '');
      }
      // 通过 app.jwt.verify + 加密字符串 解析出 token 的值
      const decode = await app.jwt.verify(token, app.config.jwt.secret);
      if (!decode) return;
      // 通过解析出的用户信息获取
      const res = await ctx.service.bill.addBill({
        amount,
        type_id,
        type_name,
        pay_type,
        remark,
        date,
        user_id: decode.id,
      });
      ctx.body = {
        code: 200,
        message: '请求成功!',
      };
    } catch (error) {
      ctx.body = {
        code: 500,
        message: '系统错误!',
      };
      console.log(error);

    }
  }
  // 获取账单列表接口
  async billList() {
    const { ctx, app } = this;
    // 获取，日期 date，分页数据，类型 type_id，这些都是我们在前端传给后端的数据
    // type_id = 'all'的时候获取全部的类型
    const { date, page = 1, page_size = 5, type_id = 'all' } = ctx.query;
    try {
      let token = ctx.request.header.authorization; // 请求头获取 authorization 属性，值为 token
      // 这里去除后才能解析出来
      if (token.indexOf('Bearer ') !== -1) {
        token = token.replace('Bearer ', '');
      }
      // 通过 app.jwt.verify + 加密字符串 解析出 token 的值
      const decode = await app.jwt.verify(token, app.config.jwt.secret);
      if (!decode) return;
      // 拿到当前用户的账单列表
      const list = await ctx.service.bill.billList(decode.id);
      const _list = list.filter(item => {
        if (type_id !== 'all') {
          return item.date.slice(0, 6) === date && type_id == item.type_id;
        }
        // 如果type_id是all返回所有的时间相同的账单
        return item.date.slice(0, 6) === date;
      });
      // 格式化数据，将其变成我们之前设置好的对象格式
      const listMap = _list.reduce((curr, item) => {
        // curr 默认初始值是一个空数组 []
        //  把第一个账单项的时间格式化为 YYYYMMDD
        const date = item.date.slice(0, 8);
        // 如果能在累加的数组中找到当前项日期 date，那么在数组中的加入当前项到 bills 数组。
        if (curr && curr.length && curr.findIndex(item => item.date.slice(0, 8) == date) > -1) {
          const index = curr.findIndex(item => item.date.slice(0, 8) == date);
          curr[index].bills.push(item);
        }
        // 如果在累加的数组中找不到当前项日期的，那么再新建一项,再把当前的数据给bills数组。
        if (curr && curr.length && curr.findIndex(item => item.date.slice(0, 8) == date) == -1) {
          curr.push({
            date,
            bills: [ item ],
          });
        }
        // 如果 curr 为空数组，则默认添加第一个账单项 item ，格式化为下列模式
        if (!curr.length) {
          curr.push({
            date,
            bills: [ item ],
          });
        }
        return curr;
      }, []).sort((a, b) => b.date - a.date); // 时间顺序为倒叙，时间约新的，在越上面
      // 分页处理，listMap 为我们格式化后的全部数据，还未分页。
      const filterListMap = listMap.slice((page - 1) * page_size, page * page_size);

      // 计算当月总收入和支出
      // 首先获取当月所有账单列表
      const __list = list.filter(item => item.date.slice(0, 6) === date);
      // 累加计算支出
      const totalExpense = __list.reduce((curr, item) => {
        if (item.pay_type == 1) {
          curr += Number(item.amount);
          return curr;
        }
        return curr;
      }, 0);
      // 累加计算收入
      const totalIncome = __list.reduce((curr, item) => {
        if (item.pay_type == 2) {
          curr += Number(item.amount);
          return curr;
        }
        return curr;
      }, 0);

      // 返回数据
      ctx.body = {
        code: 200,
        message: '请求成功',
        data: {
          totalExpense, // 当月支出
          totalIncome, // 当月收入
          totalPage: Math.ceil(listMap.length / page_size), // 总分页
          list: filterListMap || [], // 格式化后，并且经过分页处理的数据
        },
      };
    } catch (error) {
      ctx.body = {
        code: 500,
        message: '系统错误',
      };

    }

  }
  // 账单详情接口
  async details() {
    const { ctx, app } = this;
    let token = ctx.request.header.authorization; // 请求头获取 authorization 属性，值为 token
    const { id = '' } = ctx.query;
    try {
      // 这里去除后才能解析出来
      if (token.indexOf('Bearer ') !== -1) {
        token = token.replace('Bearer ', '');
      }
      // 通过 app.jwt.verify + 加密字符串 解析出 token 的值
      const decode = await app.jwt.verify(token, app.config.jwt.secret);
      if (!decode) return;
      const res = await ctx.service.bill.details(id, decode.id);
      if (!id) {
        ctx.body = {
          code: 400,
          id,
          message: 'id不能为空!',
        };
        return;
      }
      ctx.body = {
        code: 200,
        id,
        data: res,
        message: '请求成功!',
      };
    } catch (error) {
      ctx.body = {
        code: 500,
        message: '系统错误!',
      };
      console.log(error);
    }
  }
  // 修改账单接口
  async update() {
    const { ctx, app } = this;
    let token = ctx.request.header.authorization; // 请求头获取 authorization 属性，值为 token
    const { id, amount, type_id, type_name, pay_type, remark, date } = ctx.request.body;
    if (!amount || !type_id || !type_name || !pay_type || !id || !date) {
      ctx.body = {
        code: 400,
        message: '参数错误!',
      };
      return;
    }
    try {
      // 这里去除后才能解析出来
      if (token.indexOf('Bearer ') !== -1) {
        token = token.replace('Bearer ', '');
      }
      // 通过 app.jwt.verify + 加密字符串 解析出 token 的值
      const decode = await app.jwt.verify(token, app.config.jwt.secret);
      if (!decode) return;
      // 通过解析出的用户信息获取
      const res = await ctx.service.bill.update({
        id,
        amount,
        type_id,
        type_name,
        pay_type,
        remark,
        date,
        user_id: decode.id,
      });
      ctx.body = {
        code: 200,
        data: {
          id,
          amount,
          type_id,
          type_name,
          pay_type,
          remark,
          date,
          user_id: decode.id,
        },
        message: '请求成功!',
      };
    } catch (error) {
      ctx.body = {
        code: 500,
        message: '系统错误!',
      };
      console.log(error);

    }
  }
  // 修改账单接口
  async delete() {
    const { ctx, app } = this;
    let token = ctx.request.header.authorization; // 请求头获取 authorization 属性，值为 token
    const { id } = ctx.request.body;
    if (!id) {
      ctx.body = {
        code: 400,
        message: '参数错误!',
      };
      return;
    }
    try {
      // 这里去除后才能解析出来
      if (token.indexOf('Bearer ') !== -1) {
        token = token.replace('Bearer ', '');
      }
      // 通过 app.jwt.verify + 加密字符串 解析出 token 的值
      const decode = await app.jwt.verify(token, app.config.jwt.secret);
      if (!decode) return;
      // 通过解析出的用户信息获取
      const res = await ctx.service.bill.delete(id, decode.id);
      ctx.body = {
        code: 200,
        id,
        message: '请求成功!',
      };
    } catch (error) {
      ctx.body = {
        code: 500,
        message: '系统错误!',
      };
      console.log(error);

    }
  }
  // 通过日期查询账单
  async data() {
    const { ctx, app } = this;
    const { date = '' } = ctx.query;
    // 获取用户 user_id
    // 。。。
    let token = ctx.request.header.authorization; // 请求头获取 authorization 属性，值为 token
    // 省略鉴权获取用户信息的代码
    try {
      // 这里去除后才能解析出来
      if (token.indexOf('Bearer ') !== -1) {
        token = token.replace('Bearer ', '');
      }
      // 通过 app.jwt.verify + 加密字符串 解析出 token 的值
      const decode = await app.jwt.verify(token, app.config.jwt.secret);
      if (!decode) return;
      // 获取账单表中的账单数据
      const result = await ctx.service.bill.billList(decode.id);
      // 根据时间参数，筛选出当月所有的账单数据
      const start = moment(date).startOf('month').unix() * 1000; // 选择月份，月初时间
      const end = moment(date).endOf('month').unix() * 1000; // 选择月份，月末时间
      // 把我们存储的作为时间戳比较
      const _data = result.filter(item => {
        // console.log('%c294://', 'color: #FF8A00;font-size:20px;', item, moment(item.date.slice(0, 8)).unix() * 1000, moment(item.date.slice(0, 8)).unix() * 1000);
        const Start = moment(item.date.slice(0, 8)).unix() * 1000;
        const End = moment(item.date.slice(0, 8)).unix() * 1000;
        // bug:每月第一天和最后一天识别不到
        return Start + 1 > start && End - 1 < end;
      });
      // 获取收支构成
      // 总收入
      const total_income = _data.reduce((arr, cur) => {
        if (cur.pay_type == 2) {
          arr += Number(cur.amount);
        }
        return arr;
      }, 0);
      // 总支出
      const total_expense = _data.reduce((arr, cur) => {
        if (cur.pay_type == 1) {
          arr += Number(cur.amount);
        }
        return arr;
      }, 0);
      let total_data = _data.reduce((arr, cur) => {
        // fix.bug:类型 和 支出或者收入同时相等累加
        const index = arr.findIndex(item => item.type_id == cur.type_id && item.pay_type == cur.pay_type);
        // 没找到相同的type生成一条数据
        if (index == -1) {
          arr.push({
            type_id: cur.type_id,
            type_name: cur.type_name,
            pay_type: cur.pay_type,
            number: Number(cur.amount),
          });
        }
        // 如果type相同累加
        if (index > -1) {
          arr[index].number += Number(cur.amount);
        }
        return arr;
      }, []);
      // 保留两位小数
      total_data = total_data.map(item => {
        item.number = Number(item.number).toFixed(2);
        return item;
      });

      ctx.body = {
        code: 200,
        message: '请求成功',
        data: {
          total_expense: Number(total_expense).toFixed(2),
          total_income: Number(total_income).toFixed(2),
          total_data: total_data || [],
        },
      };
    } catch {
      ctx.body = {
        code: 500,
        message: '系统错误!',
        data: null,
      };
    }
  }

}
module.exports = BillController;
