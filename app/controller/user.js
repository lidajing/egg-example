'use strict';

const Controller = require('egg').Controller;
const defaultAvatar =
  '//s.yezgea02.com/1615973940679/WeChat77d6d2ac093e247c361f0b8a7aeb6c2a.png';

class UserController extends Controller {
  /**
   *
   * @return 注册接口
   */
  async register() {
    const { ctx } = this;
    const { username, password } = ctx.request.body; // 获取注册需要的参数
    // 判空操作
    if (!username || !password) {
      ctx.body = {
        code: 500,
        message: '账号密码不能为空!',
      };
      return;
    }
    const result = await ctx.service.user.getUserByName(username);
    if (result && result.id) {
      ctx.body = {
        code: 500,
        message: '用户名已被注册,请重新输入!',
      };
      return;
    }
    // 调用 service 方法，将数据存入数据库。
    // 日期类型设置为timestamp,设置默认自动递增
    const res = await ctx.service.user.register({
      username,
      password,
      signature: '世界和平。',
      avatar: defaultAvatar,
    });
    if (res) {
      ctx.body = {
        code: 200,
        message: '注册成功!',
      };
    } else {
      ctx.body = {
        code: 500,
        message: '注册失败!',
      };
    }
  }
  /**
   * 登录接口
   */
  async login() {
    const { ctx, app } = this;
    const { username, password } = ctx.request.body; // 获取注册需要的参数
    const result = await ctx.service.user.getUserByName(username);
    if (!result || !result.id) {
      ctx.body = {
        code: 500,
        message: '账号不存在!',
      };
      return;
    }
    if (result && result.password !== password) {
      ctx.body = {
        code: 500,
        message: '密码错误请重新输入!',
      };
      return;
    }
    const token = app.jwt.sign(
      {
        id: result.id,
        username: result.username,
        exp: Math.floor(Date.now() / 1000) + 24 * 60 * 60 * 7, // token 有效期为 一个星期
      },
      app.config.jwt.secret
    );
    ctx.body = {
      code: 200,
      message: '登录成功',
      data: {
        token,
      },
    };
  }
  // 验证请求接口携带的token信息是否正确
  async testToken() {
    const { ctx, app } = this;
    let token = ctx.request.header.authorization; // 请求头获取 authorization 属性，值为 token
    // 这里去除后才能解析出来
    if (token.indexOf('Bearer ') !== -1) {
      token = token.replace('Bearer ', '');
    }
    // 通过 app.jwt.verify + 加密字符串 解析出 token 的值
    const decode = await app.jwt.verify(token, app.config.jwt.secret);
    ctx.body = {
      code: 200,
      message: '操作成功!',
      data: {
        ...decode,
      },
    };
  }
  // 获取用户信息接口
  async getUserInfo() {
    const { ctx, app } = this;
    let token = ctx.request.header.authorization; // 请求头获取 authorization 属性，值为 token
    // 这里去除后才能解析出来
    if (token.indexOf('Bearer ') !== -1) {
      token = token.replace('Bearer ', '');
    }
    // 通过 app.jwt.verify + 加密字符串 解析出 token 的值
    const decode = await app.jwt.verify(token, app.config.jwt.secret);
    // 通过解析出的用户信息获取
    const res = await ctx.service.user.getUserByName(decode.username);

    if (res) {
      delete res.password;
      ctx.body = {
        code: 200,
        message: '操作成功!',
        data: {
          ...res,
        },
      };
    } else {
      ctx.body = {
        code: 500,
        message: '操作失败!',
      };
    }
  }
  // 修改用户信息
  async editUserInfo() {
    const { ctx, app } = this;
    let token = ctx.request.header.authorization; // 请求头获取 authorization 属性，值为 token
    const { signature, avatar } = ctx.request.body;
    try {
      // 这里去除后才能解析出来
      if (token.indexOf('Bearer ') !== -1) {
        token = token.replace('Bearer ', '');
      }
      // 通过 app.jwt.verify + 加密字符串 解析出 token 的值
      const decode = await app.jwt.verify(token, app.config.jwt.secret);
      if (!decode) return;
      // 通过解析出的用户信息获取
      const res = await ctx.service.user.getUserByName(decode.username);
      const userInfo = await ctx.service.user.editUserInfo({
        ...res,
        signature,
        avatar,
      });
      if (userInfo) {
        ctx.body = {
          code: 200,
          message: '操作成功!',
          data: {
            id: decode.id,
            username: decode.name,
            signature,
            avatar,
          },
        };
      } else {
        ctx.body = {
          code: 500,
          message: '操作失败!',
        };
      }
    } catch (error) {
      console.log(error);
    }
  }
  // 修改密码接口
  async editUserPas() {
    const { ctx, app } = this;
    let token = ctx.request.header.authorization; // 请求头获取 authorization 属性，值为 token
    const { password, respassword } = ctx.request.body;
    try {
      // 这里去除后才能解析出来
      if (token.indexOf('Bearer ') !== -1) {
        token = token.replace('Bearer ', '');
      }
      // 通过 app.jwt.verify + 加密字符串 解析出 token 的值
      const decode = await app.jwt.verify(token, app.config.jwt.secret);
      if (!decode) return;
      // 通过解析出的用户信息获取
      const res = await ctx.service.user.getUserByName(decode.username);
      // console.log(res, 111111111111111111);
      if (password !== res.password) {
        ctx.body = {
          code: 500,
          message: '密码错误!',
        };
        return;
      }
      const userInfo = await ctx.service.user.editUserInfo({
        ...res,
        password: respassword,
      });
      if (userInfo) {
        ctx.body = {
          code: 200,
          message: '操作成功!',
        };
      } else {
        ctx.body = {
          code: 500,
          message: '操作失败!',
        };
      }
    } catch (error) {
      console.log(error);
    }
  }
}

module.exports = UserController;
