'use strict';

const Controller = require('egg').Controller;
// const path = require('path');
// const fs = require('fs');

class HomeController extends Controller {
  async index() {
    const { ctx } = this;
    ctx.response.type = 'html';
    console.log('11111111111111');
    ctx.body = fs.readFileSync(path.resolve(__dirname, '../public/dist/index.html'));
    // ctx.body = 'hi, egg';
    // const { name } = ctx.query;
    // ctx.body = name;
    // 模板渲染页面
    await ctx.render('index.html');
  }
//   async user() {
//     const { ctx } = this;
//     const result = await ctx.service.home.user();
//     ctx.body = result;
//   }
//   async addUser() {
//     const { ctx } = this;
//     const { name } = await ctx.request.body;
//     try {
//       const result = await ctx.service.home.addUser(name);
//       ctx.body = {
//         code: 200,
//         message: '添加成功!',
//       };
//     } catch (error) {
//       ctx.body = {
//         code: 400,
//         message: '添加失败!',
//       };
//     }
//   }
//   async editUser() {
//     const { ctx } = this;
//     const { id, name } = await ctx.request.body;
//     try {
//       const result = await ctx.service.home.editUser(id, name);
//       ctx.body = {
//         code: 200,
//         message: '修改成功!',
//       };
//     } catch (error) {
//       ctx.body = {
//         code: 400,
//         message: '修改失败!',
//       };
//     }
//   }
//   async deleteUser() {
//     const { ctx } = this;
//     const { id } = await ctx.request.body;
//     try {
//       const result = await ctx.service.home.editUser(id);
//       ctx.body = {
//         code: 200,
//         message: '删除成功!',
//       };
//     } catch (error) {
//       ctx.body = {
//         code: 400,
//         message: '删除失败!',
//       };
//     }
//   }
//   async add() {
//     const { ctx } = this;
//     const { title } = ctx.request.body;
//     // Egg 框架内置了 bodyParser 中间件来对 POST 请求 body 解析成 object 挂载到 ctx.request.body 上
//     ctx.body = {
//       title,
//     };
//   }
}


module.exports = HomeController;

