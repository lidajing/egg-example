'use strict';

const moment = require('moment');
const Controller = require('egg').Controller;

class MoneyController extends Controller {
  async addMoney() {
    const { ctx, app } = this;
    let token = ctx.request.header.authorization; // 请求头获取 authorization 属性，值为 token
    const { money, updateTime } = ctx.request.body;
    try {
      // 这里去除后才能解析出来
      if (token.indexOf('Bearer ') !== -1) {
        token = token.replace('Bearer ', '');
      }
      // 通过 app.jwt.verify + 加密字符串 解析出 token 的值
      const decode = await app.jwt.verify(token, app.config.jwt.secret);
      if (!decode) return;

      const res = await ctx.service.money.addMoney({
        money,
        update_user: 1, // 默认写死
        update_time: updateTime,
      });
      if (res) {
        ctx.body = {
          code: 200,
          message: '修改成功!',
        };
      } else {
        ctx.body = {
          code: 500,
          message: '参数错误!',
        };
      }
    } catch (error) {
      ctx.body = {
        code: 500,
        message: '系统错误!',
      };
      console.log(error);
    }
  }
  async getMoney() {
    const { ctx, app } = this;
    let token = ctx.request.header.authorization; // 请求头获取 authorization 属性，值为 token
    try {
      // 这里去除后才能解析出来
      if (token.indexOf('Bearer ') !== -1) {
        token = token.replace('Bearer ', '');
      }
      // 通过 app.jwt.verify + 加密字符串 解析出 token 的值
      const decode = await app.jwt.verify(token, app.config.jwt.secret);
      if (!decode) return;
      const res = await ctx.service.money.getMoney();
      ctx.body = {
        code: 200,
        data: res,
        message: '操作成功!',
      };
    } catch (error) {
      ctx.body = {
        code: 500,
        message: '系统错误!',
      };
      console.log(error);
    }
  }
}
module.exports = MoneyController;
