'use strict';
const Controller = require('egg').Controller;
class TypeController extends Controller {
  async getType() {
    const { ctx, app } = this;
    let token = ctx.request.header.authorization; // 请求头获取 authorization 属性，值为 token

    try {
      // 这里去除后才能解析出来
      if (token.indexOf('Bearer ') !== -1) {
        token = token.replace('Bearer ', '');
      }
      // 通过 app.jwt.verify + 加密字符串 解析出 token 的值
      const decode = await app.jwt.verify(token, app.config.jwt.secret);
      if (!decode) return;
      // 通过解析出的用户信息获取
      const res = await ctx.service.type.getType();
      ctx.body = {
        code: 200,
        data: res,
        message: '请求成功!',
      };
    } catch (error) {
      ctx.body = {
        code: 500,
        message: '系统错误!',
      };
      console.log(error);

    }

  }
}
module.exports = TypeController;
