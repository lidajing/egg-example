'use strict';
// 中间件文件避免每次做重复的判断
module.exports = secret => {
  return async function jwtErr(ctx, next) {
    let token = ctx.request.header.authorization; // 若是没有 token，返回的是 null 字符串
    if (token != null && token) {
      try {
        if (token.indexOf('Bearer ') !== -1) {
          token = token.replace('Bearer ', '');
        }
        const decode = ctx.app.jwt.verify(token, secret); // 验证token
        if (decode) {
          await next();
        }
      } catch (error) {
        console.log('error', error);
        ctx.status = 200;
        ctx.body = {
          message: 'token已过期，请重新登录!',
          code: 401,
          data: null,
        };
        return;
      }
    } else {
      ctx.status = 200;
      ctx.body = {
        code: 401,
        message: 'token不存在!',
        data: null,
      };
      return;
    }
  };
};
