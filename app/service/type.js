'use strict';
const Service = require('egg').Service;

class TypeService extends Service {
  async getType() {
    const { ctx, app } = this;
    try {
      const sql = 'select * from type';
      const result = await app.mysql.query(sql);
      return result;
    } catch (error) {
      console.log(error);
      return null;
    }
  }


}
module.exports = TypeService;
