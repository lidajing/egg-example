'use strict';
const Service = require('egg').Service;

class BillService extends Service {
  // 添加账单接口
  async addBill(params) {
    const { ctx, app } = this;
    try {
      const result = await app.mysql.insert('bill', params);
      return result;
    } catch (error) {
      console.log(error);
      return null;
    }
  }
  // 获取用户账单列表(这里得到的是list)
  async billList(id) {
    const { ctx, app } = this;
    const QUERY_STR = 'id, pay_type, amount, date, type_id, type_name, remark';
    const sql = `select ${QUERY_STR} from bill where user_id = ${id}`;
    try {
      const result = await app.mysql.query(sql);
      return result;
    } catch (error) {
      console.log(error);
      return null;
    }
  }
  // 查询账单详情(得到的是对象)
  async details(id, user_id) {
    const { ctx, app } = this;
    try {
      const result = await app.mysql.get('bill', { id, user_id });
      return result;
    } catch (error) {
      console.log(error);
      return null;
    }
  }
  //  修改账单接口
  async update(params) {
    const { ctx, app } = this;
    try {
      // 表名,修改内容,查询参数
      const result = await app.mysql.update('bill', { ...params }, {
        id: params.id,
        user_id: params.user_id,
      });
      return result;
    } catch (error) {
      console.log(error);
      return null;
    }
  }
  //  删除账单接口
  async delete(id, user_id) {
    const { ctx, app } = this;
    try {
      // 表名,修改内容,查询参数安全起见加user_id
      const result = await app.mysql.delete('bill', { id, user_id });
      return result;
    } catch (error) {
      console.log(error);
      return null;
    }
  }

}
module.exports = BillService;
