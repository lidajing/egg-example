'use strict';
const Service = require('egg').Service;

class UserService extends Service {
  // 根据用户名查找用户
  async getUserByName(username) {
    const { ctx, app } = this;
    try {
      // mysql可通过app直接访问已挂载到app下
      const result = await app.mysql.get('user', { username });
      return result;
    } catch (error) {
      console.log(error);
      return null;
    }
  }
  // 注册接口
  async register(params) {
    const { ctx, app } = this;
    try {
      const result = await app.mysql.insert('user', params);
      return result;
    } catch (error) {
      console.log(error);
      return null;
    }
  }
  // 修改用户信息的接口
  async editUserInfo(params) {
    const { ctx, app } = this;
    try {
      const result = await app.mysql.update('user', { ...params }, { id: params.id });
      return result;
    } catch (error) {
      console.log(error);
      return null;
    }
  }

}

module.exports = UserService;
