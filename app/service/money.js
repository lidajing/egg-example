'use strict'
const Service = require('egg').Service

class MoneyService extends Service {
  // 修改金额,修改一次新增一条数据,做echarts
  async addMoney(params) {
    const { ctx, app } = this
    try {
      // 表名,修改内容,查询参数
      const result = await app.mysql.insert(
        'money',
        { ...params }
      )
      return result
    } catch (error) {
      console.log(error)
      return null
    }
  }
  //  获取金额
  async getMoney() {
    const { ctx, app } = this
    const QUERY_STR = 'money,update_user,update_time'
    const sql = `select ${QUERY_STR} from money`
    try {
      const result = await app.mysql.query(sql)
      return result
    } catch (error) {
      console.log(error)
      return null
    }
  }
}

module.exports = MoneyService
