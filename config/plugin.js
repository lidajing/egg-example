'use strict';

/** @type Egg.EggPlugin */
module.exports = {
  // had enabled by egg
  static: {
    enable: true,
  },
  // react: {
  //   enable: true,
  //   package: 'egg-view-react',
  // },
  // ejs: {
  //   enable: true,
  //   package: 'egg-view-ejs',
  // },
  // nunjucks: {
  //   enable: true,
  //   package: 'egg-view-nunjucks',
  // },
  mysql: {
    enable: true,
    package: 'egg-mysql',
  },
  jwt: {
    enable: true,
    package: 'egg-jwt',
  },
  // 解决跨域问题
  cors: {
    enable: true,
    package: 'egg-cors',
  },
  // io: {
  //   enable: true,
  //   package: 'egg-socket.io',
  // },
};
