/* eslint valid-jsdoc: "off" */

'use strict';

/**
 * @param {Egg.EggAppInfo} appInfo app info
 */
const path = require('path');
const fs = require('fs');
module.exports = appInfo => {
  /**
   * built-in config
   * @type {Egg.EggAppConfig}
   **/
  const config = (exports = {});

  // use for cookie sign key, should change to your own and keep security
  config.keys = appInfo.name + '_1657291724903_8917';

  // add your middleware config here
  config.middleware = [];

  // 这里配置接收post请求
  config.security = {
    csrf: {
      enable: false,
      ignoreJSON: true,
    },
    domainWhiteList: [ '*' ], // 配置白名单
  };
  // * 静态页面渲染配置
  // 将view下的.html文件,识别为.ejs
  // config.view = {
  //   mapping: { '.html': 'ejs' }, // 左边写成.html后缀，会自动渲染.html文件
  // };
  // react模板渲染配置
  // config.view = {
  //   mapping: {
  //     '.js': 'react',
  //     '.jsx': 'react',
  //   },
  // };
  // 模板渲染配置
  config.view = {
    root: [ path.join(appInfo.baseDir, 'app/view') ].join(','),
    defaultExtension: '.html',
    defaultViewEngine: 'nunjucks',
    mapping: {
      '.html': 'nunjucks',
    },
  };
  config.static = {
    // * 静态文件配置单个路径
    // prefix: '/static',
    // // dir: [ path.join(appInfo.baseDir, 'public/dist') ], // 多静态文件入口
    // dir: [ path.join(appInfo.baseDir, 'app/public/dist'), path.join(appInfo.baseDir, 'app/public/upload') ], // 多静态文件入口
    // dynamic: true,
    // preload: false,
    // * 静态文件配置多个路径
    dir: [
      { prefix: '/egg/', dir: path.join(appInfo.baseDir, 'app/public/dist') },
      { prefix: '/egg/', dir: path.join(appInfo.baseDir, 'app/public/dist/assets') },
      // 防止绝对路径； /static/xx/xx.js
      { prefix: '/static/', dir: path.join(appInfo.baseDir, 'app/public/upload') },
    ],
  };
  // ! 访问静态资源首页配置
  config.siteFile = {
    '/egg': fs.readFileSync(path.join(appInfo.baseDir, 'app/public/dist/index.html')),
    '/egg/': fs.readFileSync(path.join(appInfo.baseDir, 'app/public/dist/index.html')),
  };

  // add your user config here
  const userConfig = {
    // myAppName: 'egg',
    uploadDir: 'app/public/upload', // 上传文件夹配置
  };
  // 这里配置自定义加密字符串
  config.jwt = {
    secret: 'lijng',
  };
  // mysql 配置
  exports.mysql = {
    client: {
      // host: '124.220.6.252',
      host: 'localhost',
      port: '3306',
      user: 'root',
      // password: 'Mysql@tt1114',
      password: '12345678',
      database: 'egg_test',
    },
    // 是否加载到app上
    app: true,
    // 是否加载到agent上
    agent: false,
  };
  // 配置上传接口接收文件的形式
  config.multipart = {
    mode: 'file',
  };
  // 解决图片的跨域配置
  config.cors = {
    origin: '*', // 允许所有跨域访问
    credentials: true, // 允许 Cookie 跨域跨域
    allowMethods: 'GET,HEAD,PUT,POST,DELETE,PATCH',
  };
  return {
    ...config,
    ...userConfig,
  };
};
